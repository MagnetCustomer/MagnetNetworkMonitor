import datetime
from unittest import TestCase
from mock import patch, MagicMock

from pi_speed_monitor import speed_summary


class TestScenario(TestCase):

    def setUp(self):
        self.open = MagicMock()
        self.open_mock = patch.object(speed_summary, 'open', self.open)
        self.open_mock.start()
        self.addCleanup(self.open_mock.stop)

        csv_reader = MagicMock()

        csv_reader.DictReader.return_value = [
            {'latency': '63.382', 'speed': '10.5', 'time': '2017-08-05T14:00:00.459756'},
            {'latency': '63.382', 'speed': '10.5', 'time': '2017-08-05T14:30:21.459756'},

            {'latency': '63.382', 'speed': '12.5', 'time': '2017-08-06T14:00:00.459756'},
            {'latency': '63.382', 'speed': '12.5', 'time': '2017-08-06T14:30:21.459756'},

            {'latency': '63.382', 'speed': '2.5', 'time': '2017-08-07T14:00:00.459756'},
            {'latency': '63.382', 'speed': '0.5', 'time': '2017-08-07T14:30:21.459756'},

            {'latency': '63.382', 'speed': '4.0', 'time': '2017-08-08T14:00:00.459756'},
            {'latency': '63.382', 'speed': '3.0', 'time': '2017-08-08T14:30:21.459756'},

            {'latency': '63.382', 'speed': '3.0', 'time': '2017-08-09T14:00:00.459756'},
            {'latency': '63.382', 'speed': '10.0', 'time': '2017-08-09T14:30:21.459756'},

            {'latency': '63.382', 'speed': '2.5', 'time': '2017-08-10T14:00:00.459756'},
            {'latency': '63.382', 'speed': '2.5', 'time': '2017-08-10T14:30:21.459756'},

            {'latency': '63.382', 'speed': '3.0', 'time': '2017-08-11T14:00:00.459756'},
            {'latency': '63.382', 'speed': '2', 'time': '2017-08-11T14:30:21.459756'},

            {'latency': '63.382', 'speed': '2.5', 'time': '2017-08-12T14:00:00.459756'},
            {'latency': '63.382', 'speed': '2.5', 'time': '2017-08-12T14:30:21.459756'},

            {'latency': '63.382', 'speed': '4.5', 'time': '2017-08-13T14:00:00.459756'},
            {'latency': '63.382', 'speed': '4.5', 'time': '2017-08-13T14:30:21.459756'},

            {'latency': '63.382', 'speed': '20.0', 'time': '2017-08-14T14:00:00.459756'},
            {'latency': '63.382', 'speed': '', 'time': '2017-08-14T14:30:21.459756'},

            {'latency': '63.382', 'speed': '15.0', 'time': '2017-08-15T14:00:00.459756'},
            {'latency': '63.382', 'speed': '3.0', 'time': '2017-08-15T14:30:21.459756'},
        ]

        self.csv_reader_mock = patch.object(speed_summary, 'csv', csv_reader)
        self.csv_reader_mock.start()
        self.addCleanup(self.csv_reader_mock.stop)


class TestCalculateAverage(TestScenario):

    def test_one_day(self):
        average_speed = speed_summary.calculate_average(
            'some-path.csv',  datetime.date(2017, 8, 14), datetime.date(2017, 8, 14)
        )

        self.assertAlmostEqual(average_speed, 10.0, 2)

    def test_week(self):
        average_speed = speed_summary.calculate_average(
            'some-path.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13)
        )

        self.assertAlmostEqual(average_speed, 3.36, 2)

    def test_no_date(self):
        average_speed = speed_summary.calculate_average(
            'some-path.csv', datetime.date(2020, 1, 1), datetime.date(2020, 1, 10)
        )

        self.assertIsNone(average_speed)


class TestAverageSpeed(TestCase):

    def setUp(self):
        self.calculation = MagicMock()
        calculation_mock = patch.object(speed_summary, 'calculate_average', self.calculation)
        calculation_mock.start()
        self.addCleanup(calculation_mock.stop)

        self.post_message = MagicMock()
        post_message_patch = patch.object(speed_summary, 'post_message', self.post_message)
        post_message_patch.start()
        self.addCleanup(post_message_patch.stop)

        self.datetime = MagicMock()
        datetime_patch = patch.object(speed_summary, 'datetime', self.datetime)
        datetime_patch.start()
        self.addCleanup(datetime_patch.stop)

        self.is_file = MagicMock()
        self.is_file.path.isfile.return_value = True

        is_file_patch = patch.object(speed_summary, 'os', self.is_file)
        is_file_patch.start()
        self.addCleanup(is_file_patch.stop)

    def test_no_results(self):
        self.is_file.path.isfile.return_value = False

        speed_summary.validate_average_speed('results.csv', 'day', 24.0, 20.0)

        self.calculation.assert_not_called()
        self.post_message.assert_not_called()

    def test_day_average_fast(self):
        self.calculation.return_value = 4.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'day', 5, 4)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 13), datetime.date(2017, 8, 13))
        self.post_message.assert_not_called()

    def test_day_average_slow(self):
        self.calculation.return_value = 4.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'day', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 13), datetime.date(2017, 8, 13))
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, average speed today was 4.50MBit/s but I'm paying for 10.00Mbit/s. Sad! #MagnetNetworks #Magnet"
        )

    def test_week_average_slow(self):
        self.calculation.return_value = 4.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, average speed this week was 4.50MBit/s but I'm paying for 10.00Mbit/s. Sad! #MagnetNetworks #Magnet"
        )

    def test_week_average_fast(self):
        self.calculation.return_value = 20
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))
        self.post_message.assert_not_called()

    def test_week_results_monday(self):
        self.calculation.return_value = 20
        self.datetime.date.today.return_value = datetime.date(2017, 8, 7)

        speed_summary.validate_average_speed('results.csv', 'week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 7))

    def test_week_results_thursday(self):
        self.calculation.return_value = 20
        self.datetime.date.today.return_value = datetime.date(2017, 8, 10)

        speed_summary.validate_average_speed('results.csv', 'week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 10))

    def test_week_results_sunday(self):
        self.calculation.return_value = 20
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))

    def test_previous_day(self):
        self.calculation.return_value = 8.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'yesterday', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 12), datetime.date(2017, 8, 12))
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, average speed yesterday was 8.50MBit/s but I'm paying for 10.00Mbit/s. Sad! #MagnetNetworks #Magnet"
        )

    def test_previous_day_no_measurements(self):
        self.calculation.return_value = None
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'yesterday', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 12), datetime.date(2017, 8, 12))
        self.post_message.assert_not_called()

    def test_previous_day_no_tweet(self):
        self.calculation.return_value = 9.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)

        speed_summary.validate_average_speed('results.csv', 'yesterday', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 12), datetime.date(2017, 8, 12))
        self.post_message.assert_not_called()

    def test_previous_week(self):
        self.calculation.return_value = 8.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 15)

        speed_summary.validate_average_speed('results.csv', 'previous-week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, average speed previous week was 8.50MBit/s but I'm paying for 10.00Mbit/s. Sad! #MagnetNetworks #Magnet"
        )

    def test_previous_week_sunday_no_tweet(self):
        self.calculation.return_value = 9.5
        self.datetime.date.today.return_value = datetime.date(2017, 8, 20)

        speed_summary.validate_average_speed('results.csv', 'previous-week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))
        self.post_message.assert_not_called()

    def test_previous_week_monday_tweet(self):
        self.calculation.return_value = 4.53
        self.datetime.date.today.return_value = datetime.date(2017, 8, 14)

        speed_summary.validate_average_speed('results.csv', 'previous-week', 10.0, 9)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13))
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, average speed previous week was 4.53MBit/s but I'm paying for 10.00Mbit/s. Sad! #MagnetNetworks #Magnet"
        )
