import datetime
from unittest import TestCase
from mock import patch, MagicMock

from pi_speed_monitor import downtime_summary


class TestScenario(TestCase):

    def setUp(self):
        self.open = MagicMock()
        self.open_mock = patch.object(downtime_summary, 'open', self.open)
        self.open_mock.start()
        self.addCleanup(self.open_mock.stop)

        csv_reader = MagicMock()

        csv_reader.DictReader.return_value = [
            {'latency': '', 'speed': '', 'time': '2017-08-05T14:00:00.459756'},
            {'latency': '63.382', 'speed': '10.5', 'time': '2017-08-05T14:30:21.459756'},

            {'latency': '63.382', 'speed': '12.5', 'time': '2017-08-06T14:00:00.459756'},
            {'latency': '63.382', 'speed': '12.5', 'time': '2017-08-06T14:30:21.459756'},

            {'latency': '', 'speed': '', 'time': '2017-08-07T14:00:00.459756'},
            {'latency': '63.382', 'speed': '0.5', 'time': '2017-08-07T14:30:21.459756'},

            {'latency': '63.382', 'speed': '4.0', 'time': '2017-09-01T14:00:00.459756'},
            {'latency': '63.382', 'speed': '3.0', 'time': '2017-09-02T14:30:21.459756'},

            {'latency': '', 'speed': '', 'time': '2017-10-01T14:00:00.459756'},
            {'latency': '', 'speed': '', 'time': '2017-10-02T14:30:21.459756'},
            {'latency': '50.0', 'speed': '10.0', 'time': '2017-10-02T15:00:21.459756'},
            {'latency': '', 'speed': '', 'time': '2017-10-03T15:30:21.459756'},
        ]

        self.csv_reader_mock = patch.object(downtime_summary, 'csv', csv_reader)
        self.csv_reader_mock.start()
        self.addCleanup(self.csv_reader_mock.stop)


class TestCalculateDowntime(TestScenario):

    def test_one_day(self):
        downtime = downtime_summary.calculate_downtime(
            'some-path.csv',  datetime.date(2017, 8, 5), datetime.date(2017, 8, 5), 15
        )

        self.assertEquals(downtime, 15)

    def test_two_days(self):
        downtime = downtime_summary.calculate_downtime(
            'some-path.csv', datetime.date(2017, 8, 20), datetime.date(2017, 9, 20), 15
        )

        self.assertEquals(downtime, 0)

    def test_no_downtime(self):
        downtime = downtime_summary.calculate_downtime(
            'some-path.csv', datetime.date(2017, 10, 1), datetime.date(2017, 10, 2), 15
        )

        self.assertEquals(downtime, 30)


class TestAverageSpeed(TestCase):

    def setUp(self):
        self.calculation = MagicMock()
        self.calculation.return_value = 0
        calculation_mock = patch.object(downtime_summary, 'calculate_downtime', self.calculation)
        calculation_mock.start()
        self.addCleanup(calculation_mock.stop)

        self.post_message = MagicMock()
        post_message_patch = patch.object(downtime_summary, 'post_message', self.post_message)
        post_message_patch.start()
        self.addCleanup(post_message_patch.stop)

        self.datetime = MagicMock()
        datetime_patch = patch.object(downtime_summary, 'datetime', self.datetime)
        datetime_patch.start()
        self.addCleanup(datetime_patch.stop)

        self.is_file = MagicMock()
        self.is_file.path.isfile.return_value = True

        is_file_patch = patch.object(downtime_summary, 'os', self.is_file)
        is_file_patch.start()
        self.addCleanup(is_file_patch.stop)

    def test_no_results_file(self):
        self.is_file.path.isfile.return_value = False

        downtime_summary.measure_downtime('results.csv', 'week', 60, 15)

        self.calculation.assert_not_called()
        self.post_message.assert_not_called()

    def test_week_on_monday(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 7)

        downtime_summary.measure_downtime('results.csv', 'week', 60, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 7), 15)
        self.post_message.assert_not_called()

    def test_week_on_monday_post_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 7)
        self.calculation.return_value = 90

        downtime_summary.measure_downtime('results.csv', 'week', 60, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 7), 15)
        self.post_message.assert_called_once_with("@MagnetNetworks, this week I suffered 1.50 hour(s) of downtime. Sad! #MagnetNetworks #Magnet")

    def test_week_on_sunday_post_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 13)
        self.calculation.return_value = 90

        downtime_summary.measure_downtime('results.csv', 'week', 60, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13), 15)
        self.post_message.assert_called_once_with("@MagnetNetworks, this week I suffered 1.50 hour(s) of downtime. Sad! #MagnetNetworks #Magnet")

    def test_previous_month_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 1)
        self.calculation.return_value = 300

        downtime_summary.measure_downtime('results.csv', 'previous-month', 270, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 7, 1), datetime.date(2017, 7, 31), 15)
        self.post_message.assert_called_once_with("@MagnetNetworks, last month I suffered 5.00 hour(s) of downtime. Sad! #MagnetNetworks #Magnet")

    def test_previous_month_no_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 1)
        self.calculation.return_value = 0

        downtime_summary.measure_downtime('results.csv', 'previous-month', 270, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 7, 1), datetime.date(2017, 7, 31), 15)
        self.post_message.assert_not_called()

    def test_previous_month_february_no_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 3, 10)
        self.calculation.return_value = 0

        downtime_summary.measure_downtime('results.csv', 'previous-month', 270, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 2, 1), datetime.date(2017, 2, 28), 15)
        self.post_message.assert_not_called()

    def test_previous_month_last_day_no_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 3, 31)
        self.calculation.return_value = 0

        downtime_summary.measure_downtime('results.csv', 'previous-month', 270, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 2, 1), datetime.date(2017, 2, 28), 15)
        self.post_message.assert_not_called()

    def test_previous_month_new_year_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2018, 1, 1)
        self.calculation.return_value = 360

        downtime_summary.measure_downtime('results.csv', 'previous-month', 270, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 12, 1), datetime.date(2017, 12, 31), 15)
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, last month I suffered 6.00 hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
        )

    def test_previous_week(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 15)
        self.calculation.return_value = 210

        downtime_summary.measure_downtime('results.csv', 'previous-week', 1, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13), 15)
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, previous week I suffered 3.50 hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
        )

    def test_previous_week_sunday_no_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 20)
        self.calculation.return_value = 210

        downtime_summary.measure_downtime('results.csv', 'previous-week', 300, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13), 15)
        self.post_message.assert_not_called()

    def test_previous_week_monday_tweet(self):
        self.datetime.date.today.return_value = datetime.date(2017, 8, 14)
        self.calculation.return_value = 180

        downtime_summary.measure_downtime('results.csv', 'previous-week', 100, 15)

        self.calculation.assert_called_once_with('results.csv', datetime.date(2017, 8, 7), datetime.date(2017, 8, 13), 15)
        self.post_message.assert_called_once_with(
            "@MagnetNetworks, previous week I suffered 3.00 hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
        )