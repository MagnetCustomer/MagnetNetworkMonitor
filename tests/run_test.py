import datetime
from unittest import TestCase
from mock import patch, MagicMock

from pi_speed_monitor import run_test


class TestStoreResults(TestCase):

    def setUp(self):
        self.open = MagicMock()
        open_patcher = patch.object(run_test, 'open', self.open)
        open_patcher.start()
        self.addCleanup(open_patcher.stop)

        self.get_speed = MagicMock()
        get_results_patcher = patch.object(run_test, '_get_speed', self.get_speed)
        get_results_patcher.start()
        self.addCleanup(get_results_patcher.stop)

        self.os_mock = MagicMock()
        self.os_mock.path.isfile.return_value = True

        os_patcher = patch.object(run_test, 'os', self.os_mock)
        os_patcher.start()
        self.addCleanup(os_patcher.stop)

        self.writer = MagicMock()
        csv_mock = MagicMock()
        csv_mock.DictWriter.return_value = self.writer
        csv_patcher = patch.object(run_test, 'csv', csv_mock)
        csv_patcher.start()
        self.addCleanup(csv_patcher.stop)

        self.datetime = MagicMock()
        self.datetime.datetime.now.return_value = datetime.datetime(2017, 2, 7)
        datetime_patcher = patch.object(run_test, 'datetime', self.datetime)
        datetime_patcher.start()
        self.addCleanup(datetime_patcher.stop)

    def test_saves_result(self):
        self.get_speed.return_value = {
            'latency': 10.0,
            'speed': 5.00,
        }

        run_test.measure('/some_path.csv')

        self.writer.writerow.assert_called_once_with({
            'latency': 10.0,
            'speed': 5.0,
            'time': '2017-02-07T00:00:00'
        })

    def test_handles_exeption(self):
        self.get_speed.side_effect = Exception('some exception')

        run_test.measure('/some_path.csv')

        self.writer.writerow.assert_called_once_with({
            'latency': None,
            'speed': None,
            'time': '2017-02-07T00:00:00'
        })

    def test_handles_zero_speed(self):
        self.get_speed.return_value = {
            'latency': 8000.0,
            'speed': 0.05,
            'time': '2017-02-07T00:00:00'
        }

        run_test.measure('/some_path.csv')

        self.writer.writerow.assert_called_once_with({
            'latency': None,
            'speed': None,
            'time': '2017-02-07T00:00:00'
        })

