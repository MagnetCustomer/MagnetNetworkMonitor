import os

from twitter import Twitter, OAuth


def post_message(message):
    t = Twitter(auth=OAuth(
        os.environ['MAGNET_TWITTER_TOKEN'], os.environ['MAGNET_TWITTER_TOKEN_SECRET'],
        os.environ['MAGNET_TWITTER_CONSUMER_KEY'], os.environ['MAGNET_TWITTER_CONSUMER_SECRET']
    ))

    t.statuses.update(status=message)
