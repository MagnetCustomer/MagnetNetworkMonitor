import argparse
import csv
import datetime
import os

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from .utils import post_message


def calculate_average(path, date_from, date_to):
    with open(path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile)

        measurements = []

        for row in reader:
            measurement_date = parse(row['time']).date()

            if date_from <= measurement_date and measurement_date <= date_to:
                if row['speed']:
                    measurements.append(float(row['speed']))
                else:
                    measurements.append(0.0)

        if measurements:
            return sum(measurements) / len(measurements)


def validate_average_speed(path, period, expected, minimum):
    if not os.path.isfile(path):
        return

    if period == 'day':
        from_date = datetime.date.today()
        to_date = from_date

        message = "@MagnetNetworks, average speed today was {0:.2f}MBit/s but I'm paying for {1:.2f}Mbit/s. Sad! #MagnetNetworks #Magnet"
    elif period == 'week':
        current_date = datetime.date.today()

        from_date = current_date - relativedelta(days=current_date.weekday())
        to_date = current_date

        message = "@MagnetNetworks, average speed this week was {0:.2f}MBit/s but I'm paying for {1:.2f}Mbit/s. Sad! #MagnetNetworks #Magnet"
    elif period == 'yesterday':
        from_date = datetime.date.today() - relativedelta(days=1)
        to_date = from_date

        message = "@MagnetNetworks, average speed yesterday was {0:.2f}MBit/s but I'm paying for {1:.2f}Mbit/s. Sad! #MagnetNetworks #Magnet"
    elif period == 'previous-week':
        previous_week = datetime.date.today() - relativedelta(days=7)
        from_date = previous_week - relativedelta(days=previous_week.weekday())
        to_date = from_date + relativedelta(days=6)

        message = "@MagnetNetworks, average speed previous week was {0:.2f}MBit/s but I'm paying for {1:.2f}Mbit/s. Sad! #MagnetNetworks #Magnet"
    else:
        raise Exception('Invalid period')

    average_speed = calculate_average(path, from_date, to_date)

    if average_speed is not None and average_speed < minimum:
        post_message(message.format(average_speed, expected))


def main(**kwargs):
    parser = argparse.ArgumentParser(description="post a tweet with average result speed")
    parser.add_argument('--path', required=True, dest='path',
                        help='path where to store the results')
    parser.add_argument('--period', required=True, dest='period', choices=['day', 'week', 'yesterday', 'previous-week'],
                        help='period to measure the speed')
    parser.add_argument('--expected', required=True, dest='expected', type=float,
                        help='expected speed (mbits)')
    parser.add_argument('--minimum', required=True, dest='minimum', type=float,
                        help='if speed is slower than the minimum speed, trigger a tweet')

    args = parser.parse_args()
    validate_average_speed(args.path, args.period, args.expected, args.minimum)
