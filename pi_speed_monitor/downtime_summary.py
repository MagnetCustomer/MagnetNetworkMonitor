import argparse
import csv
import datetime
import os

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from .utils import post_message


def calculate_downtime(path, date_from, date_to, measurement_interval):
    with open(path, 'rt') as csvfile:
        reader = csv.DictReader(csvfile)

        downtime_instances = 0

        for row in reader:
            measurement_date = parse(row['time']).date()

            if date_from <= measurement_date and measurement_date <= date_to:
                if not row['speed'] and not row['latency']:
                    downtime_instances += 1

        return downtime_instances * measurement_interval


def measure_downtime(path, period, minimum, measurement_interval):
    if not os.path.isfile(path):
        return

    if period == 'week':
        current_date = datetime.date.today()

        from_date = current_date - relativedelta(days=current_date.weekday())
        to_date = current_date

        message = "@MagnetNetworks, this week I suffered {0:.2f} hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
    elif period == 'previous-month':
        current_date = datetime.date.today()

        from_date = current_date.replace(day=1) - relativedelta(months=1)
        to_date = current_date.replace(day=1) - relativedelta(days=1)

        message = "@MagnetNetworks, last month I suffered {0:.2f} hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
    elif period == 'previous-week':
        previous_week = datetime.date.today() - relativedelta(days=7)
        from_date = previous_week - relativedelta(days=previous_week.weekday())
        to_date = from_date + relativedelta(days=6)

        message = "@MagnetNetworks, previous week I suffered {0:.2f} hour(s) of downtime. Sad! #MagnetNetworks #Magnet"
    else:
        raise Exception('Invalid period')

    total_downtime = calculate_downtime(path, from_date, to_date, measurement_interval)

    if total_downtime >= minimum:
        total_downtime = total_downtime / 60.0
        post_message(message.format(total_downtime))


def main():
    parser = argparse.ArgumentParser(description="post a tweet about total downtime experienced")

    parser.add_argument('--path', required=True, dest='path',
                        help='path where to store the results')

    parser.add_argument('--period', required=True, dest='period', choices=['week', 'previous-month', 'previous-week'],
                        help='period to measure the speed')

    parser.add_argument('--minimum-downtime', required=True, dest='minimum_downtime', type=float,
                        help='Minimum downtime (in minutes) that should trigger a tweet')

    parser.add_argument('--measurement-interval', required=True, dest='measurement_interval', type=int,
                        help='Time between measurements (minutes)')

    args = parser.parse_args()
    measure_downtime(args.path, args.period, args.minimum_downtime, args.measurement_interval)
