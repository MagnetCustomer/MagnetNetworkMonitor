import argparse
import csv
import datetime
import os
from multiprocessing.pool import ThreadPool

from speedtest import Speedtest


def _get_speed():
    # Running the measurement in a separate thread, since the download method
    # has no timeout option, and we don't want our process hanging, for too long.
    # Applying a *very* generous timeout of 2 minutes
    def _measure():
        s = Speedtest()
        server = s.get_best_server()

        speed = s.download() / (1024.0 * 1024.0)

        return {
            'latency': server['latency'],
            'speed': speed,

        }

    pool = ThreadPool(processes=1)

    measurement_async = pool.apply_async(_measure)

    measurement = measurement_async.get(120)
    return measurement


def measure(path):
    measurement_time = datetime.datetime.now().isoformat()

    try:
        results = _get_speed()

        if results['speed'] <= 0.05:
            raise Exception()

        results['time'] = measurement_time
    except:
        results = {
            'latency': None,
            'speed': None,
            'time': measurement_time
        }

    write_headers = not os.path.isfile(path)

    with open(path, 'at') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=['time', 'latency', 'speed'])

        if write_headers:
            writer.writeheader()

        writer.writerow(results)


def main(**kwargs):

    if kwargs:
        measure(kwargs['path'])
    else:
        parser = argparse.ArgumentParser(description="measure connection's speed and store the results")
        parser.add_argument('--path', required=True, dest='path',
                            help='path where to store the results')

        args = parser.parse_args()
        measure(args.path)


if __name__ == '__main__':
    main(path='results.csv')
