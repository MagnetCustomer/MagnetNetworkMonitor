from setuptools import setup


setup(
    name='pi_speed_monitor',
    version='0.3.2',
    author='Magnet Customer',
    author_email='magnetcustomer@protonmail.ch',
    packages=['pi_speed_monitor',],
    install_requires=[
        'speedtest-cli',
        'twitter',
        'python-dateutil',
    ],
    entry_points={
        'console_scripts': [
            'measure-speed = pi_speed_monitor.run_test:main',
            'post-speed-summary-tweet = pi_speed_monitor.speed_summary:main',
            'post-downtime-summary-tweet = pi_speed_monitor.downtime_summary:main',
        ],
    },
    extras_require={
        'dev': [
            'pytest',
            'mock',
            'ansible',
            'tox',
        ]
    }
)
