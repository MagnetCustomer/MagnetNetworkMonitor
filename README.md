# Magnet Network Monitor

A small utility to be run on Raspberry Pi to monitor network's speed and send tweets to [Magnet Networks](https://www.magnetnetworks.com/). The app measures the connection's speed and latency every 15 minutes. It later generates the following status reports: 
- Average connection's speed per day, week, and month
- Total downtime suffered per week and month

If the average speed or downtime reaches certain thresholds, a tweet is sent to [Magnet Networks' Twitter account](https://www.magnetnetworks.com/).

The repository also contains Ansible scripts to deploy the app. You will need to update the hosts file to match your setup if you want to run it yourself.

The project was born out of frustration with Magnet's poor network and customer service. 

